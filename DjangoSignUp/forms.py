from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.validators import RegexValidator


class UserSignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    #contact = forms.CharField(max_length=10)
    vals=RegexValidator(regex=r'[0-9]{10}',message="invalid contact")
    contact=forms.CharField(max_length=10,validators=[vals])
    city = forms.CharField(max_length=20)

    class Meta:
        model = User
        fields = ('email', 'contact', 'city', 'password1', 'password2', )
