from django.conf.urls import url
from django.urls import path
from django.contrib import admin
from . import views
from django.conf import settings
from django.conf.urls import static


urlpatterns = [
    path('', views.Register, name='Register'),
    url(r'^admin/', admin.site.urls),
]+static.static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
