# -*- coding: utf-8 -*-
from django.core.mail import EmailMessage
from celery import shared_task, states


@shared_task()
def sendemail(sub,msg,to):
    try:
        mail_subject = sub
        message = msg
        email = EmailMessage(mail_subject, message, to=[to])
        email.send(fail_silently=False)
        print('send')
        return ('success')
    except Exception as e:
        print(e)
