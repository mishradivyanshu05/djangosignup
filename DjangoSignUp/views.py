from django.http import HttpResponse
from django.shortcuts import render,redirect
from django.core.mail import EmailMessage
from django.contrib.auth.models import User
from .models import UserDetails
from .forms import UserSignUpForm
from .tasks import sendemail

def Register(request):
    if request.method == 'POST':
        form=UserSignUpForm(request.POST)
        if form.is_valid():
            password1 = form.cleaned_data.get("password1")
            email=form.cleaned_data.get("email")
            contact=form.cleaned_data.get("contact")
            city = form.cleaned_data.get('city')
            print(city)
            useremail_exists=User.objects.filter(email=email).exists()
            usercontact_exists=UserDetails.objects.filter(contact=contact).exists()
            if useremail_exists:
                error = "User with this email already exists."
                form = UserSignUpForm()
                return render(request, 'signup.html',{'error':error,'form':form})
            if usercontact_exists:
                error = "User with this contact already exists."
                form = UserSignUpForm()
                return render(request, 'signup.html',{'error':error,'form':form})
            user=User.objects.create_user(username=email,password=password1,email=email)
            userdet = UserDetails.objects.create(user=user,contact=contact,city=city)
            user.is_active = True
            sub = 'Registration Success'
            msg = 'Thanks for Registration'
            sendemail.delay(sub,msg,email)
            user.save()
            userdet.save()
            return HttpResponse("Registration Success")
    else:
        form = UserSignUpForm()
    return render(request, 'signup.html', {'form': form})
