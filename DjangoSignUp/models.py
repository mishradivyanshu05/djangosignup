from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator

class UserDetails(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    vals=RegexValidator(regex=r'[0-9]{10}',message="invalid contact")
    contact=models.CharField(max_length=10,validators=[vals])
    city = models.CharField(max_length=20,null=True,blank=True)
